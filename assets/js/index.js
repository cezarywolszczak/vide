var btnStartRecording = document.querySelector('#btn-start-recording');
var btnStopRecording  = document.querySelector('#btn-stop-recording');
var videoElement      = document.querySelector('video');
var progressBar = document.querySelector('#progress-bar');
var percentage = document.querySelector('#percentage');

var recorder;

function postFiles() {
    var blob = recorder.getBlob();
    var fileName = generateRandomString() + '.webm';
    var file = new File([blob], fileName, {
        type: 'video/webm'
    });
    videoElement.src = '';
    xhr('/uploadFile', file, function(responseText) {
        var fileURL = JSON.parse(responseText).fileURL;
        console.info('fileURL', fileURL);
        console.info(JSON.parse(responseText).files);
        videoElement.src = fileURL;
        videoElement.play();
        videoElement.muted = false;
        videoElement.controls = true;
        document.querySelector('#footer-h2').innerHTML = '<a href="' + videoElement.src + '">' + videoElement.src + '</a>';
    });
    
    if(mediaStream) mediaStream.stop();
}

function xhr(url, data, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
            callback(request.responseText);
        }
    };
            
    request.open('POST', url);
    var formData = new FormData();
    formData.append('file', data);
    request.send(formData);
}
function generateRandomString() {
    if (window.crypto) {
        var a = window.crypto.getRandomValues(new Uint32Array(3)),
            token = '';
        for (var i = 0, l = a.length; i < l; i++) token += a[i].toString(36);
        return token;
    } else {
        return (Math.random() * new Date().getTime()).toString(36).replace( /\./g , '');
    }
}
var mediaStream = null;
function captureUserMedia(success_callback) {
    var session = {
        audio: true,
        video: true
    };
    
    navigator.getUserMedia(session, success_callback, function(error) {
        alert('Unable to capture your camera. Please check console logs.');
        console.error(error);
    });
}
btnStartRecording.onclick = function() {
    btnStartRecording.disabled = true;
    
    captureUserMedia(function(stream) {
        mediaStream = stream;
        
        videoElement.src = window.URL.createObjectURL(stream);
        videoElement.play();
        videoElement.muted = true;
        videoElement.controls = false;
        
        recorder = RecordRTC(stream, {
            type: 'video'
        });
        
        recorder.startRecording();
        btnStopRecording.disabled = false;
    });
};
btnStopRecording.onclick = function() {
    btnStartRecording.disabled = false;
    btnStopRecording.disabled = true;
    
    recorder.stopRecording(postFiles);
};
window.onbeforeunload = function() {
    startRecording.disabled = false;
};


window.onload = function() {
    var text;
    var client = new XMLHttpRequest();
    var tableOfMoveNames;
    client.open('GET', '../../uploads/list.txt');
    client.onreadystatechange = function() {
        text = client.responseText;
        tableOfMoveNames = text.split(';');
        tableOfMoveNames = tableOfMoveNames.splice(0, tableOfMoveNames.length - 1);
        tableOfMoveNames.reverse();

        var htmlCode = '';
        var filmContainer = document.getElementById('films-container');
        [].forEach.call(tableOfMoveNames, (element, index)=>{
            htmlCode += '<video class="videos" name="media" controls><source src="./uploads/'+ element + '" type="video/webm"></video>';
        });
        filmContainer.innerHTML = htmlCode;
        console.log(tableOfMoveNames)
    }
    client.send();
    
}